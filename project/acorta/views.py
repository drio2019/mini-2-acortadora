from django.http import HttpResponse, Http404, HttpResponseRedirect
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from .models import Shortener
from django.template import loader


# Create your views here.


@csrf_exempt
def index(request):
    new_shorted_url = ''
    if request.method == "POST":
        url, short = get_parameters(request)
        if not url:  # Si se envia una peticion con una URL erronea
            template, context = set_template(new_shorted_url)
            return HttpResponse(template.render(context, request))
        port = request.META['SERVER_PORT']
        shorted_url = "http://localhost:" + str(port) + "/"
        # Si existe la URL original pero no la URL acortada
        if original_url_exists(url) and not shorted_url_exists(shorted_url + short):
            new_shorted_url = switch_shorted(short, shorted_url, url)
        # Si existe valor acortado, crearemos una URL original si que se repita y una URL acortada con el valor acortado
        elif short:
            shorted_url += short
            try:
                if original_url_exists(url):  # Se evitar crear un nuevo enlace si existe uno
                    old_url = Shortener.objects.get(original_url=url)
                    old_url.delete()
                new_shorted_url = Shortener.objects.get(shorted_url=shorted_url)
                new_shorted_url.original_url = url
            except Shortener.DoesNotExist:
                new_shorted_url = Shortener(shorted_url=shorted_url, original_url=url)
        # Si no existe valor acortado (vacio), crearemos una URL original y una URL acortada con un contador
        else:
            shorted_url += str(get_counter())
            new_shorted_url = Shortener(shorted_url=shorted_url, original_url=url)
        new_shorted_url.save()
    template, context = set_template(new_shorted_url)
    return HttpResponse(template.render(context, request))


@csrf_exempt
def redirect(request, key):
    port = request.META['SERVER_PORT']
    shorted_url = "http://localhost:" + str(port) + "/" + key
    try:
        url = Shortener.objects.get(shorted_url=shorted_url)
    except Shortener.DoesNotExist:
        raise Http404("URL not found")
    return HttpResponseRedirect(url.original_url)


def switch_shorted(short, shorted_url, original_url):
    new_shorted_url = Shortener.objects.get(original_url=original_url)
    if short:
        new_shorted_url.shorted_url = shorted_url + short
    else:
        new_shorted_url.shorted_url = shorted_url + str(get_counter())
    new_shorted_url.save()
    return new_shorted_url


# Comprueba si la URL original existe en la base de datos
def original_url_exists(original_url):
    try:
        Shortener.objects.get(original_url=original_url)
        return True
    except Shortener.DoesNotExist:
        return False


# Comprueba si la URL acortada existe en la base de datos
def shorted_url_exists(shorted_url):
    try:
        Shortener.objects.get(shorted_url=shorted_url)
        return True
    except Shortener.DoesNotExist:
        return False


def get_counter():
    counter = 0  # Inicializamos el contador en 0
    list_shortener = Shortener.objects.all()  # Devuelve todos los objetos de la base de datos
    for url in list_shortener:  # Recorre la lista de objetos de la base de datos (fila por fila)
        try:
            aux_counter = int(url.shorted_url.split('/')[-1])
        except ValueError:
            aux_counter = 0
        if aux_counter > counter:
            counter = aux_counter
    counter += 1
    return counter


def set_template(current_url):
    list_shortener = Shortener.objects.all()
    template = loader.get_template('index.html')
    context = {
        'list_shortener': list_shortener,
        'current_url': current_url,
    }
    return template, context


def get_parameters(request):
    url = request.POST.get('url')
    short = request.POST.get('short')
    if not url.startswith('http://') and not url.startswith('https://'):
        url = "https://" + url
    return url, short
